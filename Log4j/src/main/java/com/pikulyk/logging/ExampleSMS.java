package com.pikulyk.logging;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {

  // Find your Account Sid and Token at twilio.com/user/account
  public static final String ACCOUNT_SID = "AC2633ce2f7beb1c9b787360cbcde76a6e";
  public static final String AUTH_TOKEN = "a50d8ae21f4121aa8caa5d00b0e4efe6";

  public static void send(String str) {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    Message message = Message
        .creator(new PhoneNumber("+380*******"), /*my phone number*/
            new PhoneNumber("+17275130988"), str).create(); /*attached to me number*/
  }
}